﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Patten1
{
    public interface IQUERYSQL
    {
        void SetQueryLevel1();
        void SetQueryLevel2(List<string> ListSelect);
        void SetQueryLevel3();
        void SetQueryLevel4(string tableName);
        void SetQueryLevel5();
        void SetQueryLevel6(Dictionary<string, string> attSelect);
        void SetQueryLevel7();
        void SetQueryLevel8(string columnGroupBy);
        void SetQueryLevel9();
        void SetQueryLevel10(string conditionHaving);
        void SetQueryLevel11(Dictionary<string, string> valueInsert);
        void SetQueryLevel12(Dictionary<string, string> valueInsert);
        QUERYSQL GetSQL();


    }
    public class QUERYSQL
    {
        public List<string> command { get; set; }
        public List<string> listSelect { get; set; }
        public string tableName { get; set; }
        public Dictionary<string,string> condition { get; set; }
        public string ColumnGroupby { get; set; }
        public string conditionHaving { get; set; }
        public Dictionary<string,string> valueInsert { get; set; }
        public Dictionary<string, string> valueUpdate { get; set; }
        public QUERYSQL()
        {
            command = new List<string>();
            listSelect = new List<string>();
            condition = new Dictionary<string, string>();
            valueInsert = new Dictionary<string, string>();
            valueUpdate = new Dictionary<string, string>();
        }


        public string QueryInsert()
        {
            return this.command[0]
                + ((this.listSelect == null) ? "" : String.Join(",", this.listSelect))
                + this.command[1]
                + this.tableName
                + ((this.valueUpdate== null) ? "" : ("\nSET" + " " + String.Join(",", String.Join(",", this.valueUpdate.Select(x => x.Key + "=" + x.Value)))))
                + ((this.valueInsert == null) ? "" : "(" + String.Join(",", this.valueInsert.Select(x => x.Key)) + ")")
                + ((this.condition == null) ? "" : (this.command[2] + " " + String.Join(" AND ", this.condition.Select(x => x.Key + "=" + x.Value))))
                +((this.valueInsert == null) ? "" : (this.command[2] + " (" + String.Join(",", this.valueInsert.Select(x => x.Value)) + ")"))
                +((string.IsNullOrEmpty(this.ColumnGroupby)) ? "" : (this.command[3] + ColumnGroupby))
                + ((string.IsNullOrEmpty(this.conditionHaving)) ? "" : (this.command[4] + conditionHaving));
            }
    }

    public class SELECTBUILDER : IQUERYSQL
    {
        QUERYSQL QUERYSQL = new QUERYSQL();
        public void SetQueryLevel1()
        {
            QUERYSQL.command.Add("SELECT ");
        }
        public void SetQueryLevel2(List<string> listSelect)
        {
            QUERYSQL.listSelect = listSelect;
        }
        public void SetQueryLevel3()
        {
            QUERYSQL.command.Add("\nFROM ");
        }
        public void SetQueryLevel4(string tableName)
        {
            QUERYSQL.tableName = tableName;
        }
        public void SetQueryLevel5()
        {
            QUERYSQL.command.Add("\nWHERE ");
        }
        public void SetQueryLevel6(Dictionary<string, string> condition)
        {
            QUERYSQL.condition = condition;
        }
        public void SetQueryLevel7()
        {
            QUERYSQL.command.Add("\nGROUP BY ");
        }

        public void SetQueryLevel8(string columGroupBy)
        {
            QUERYSQL.ColumnGroupby = columGroupBy;
        }

        public void SetQueryLevel9()
        {
            QUERYSQL.command.Add("\nHAVING ");
        }
        public void SetQueryLevel10(string conditionHaving)
        {
            QUERYSQL.conditionHaving = conditionHaving;
        }
        public void SetQueryLevel11(Dictionary<string, string> valueInsert)
        {
          
        }
        public void SetQueryLevel12(Dictionary<string, string> valueUpdate)
        {
           
        }
        public QUERYSQL GetSQL()
        {

            return QUERYSQL;
        }
    }
    public class INSERTBUILDER:IQUERYSQL
    {
        QUERYSQL QUERYSQL = new QUERYSQL();
        public void SetQueryLevel1()
        {
            QUERYSQL.command.Add("INSERT ");
        }
        public void SetQueryLevel2(List<string> listSelect)
        {
            QUERYSQL.listSelect = null;
        }
        public void SetQueryLevel3()
        {
            QUERYSQL.command.Add("\nINTO ");
        }
        public void SetQueryLevel4(string tableName)
        {
            QUERYSQL.tableName = tableName;
        }
        public void SetQueryLevel5()
        {
            QUERYSQL.command.Add("\nVALUES ");
        }
        public void SetQueryLevel6(Dictionary<string, string> condition)
        {
            QUERYSQL.condition = null;
        }
        public void SetQueryLevel7()
        {
     
        }

        public void SetQueryLevel8(string columGroupBy)
        {
       
        }

        public void SetQueryLevel9()
        {
       
        }
        public void SetQueryLevel10(string conditionHaving)
        {
  
        }
        public void SetQueryLevel11(Dictionary<string, string> valueInsert)
        {
            QUERYSQL.valueInsert = valueInsert;
        }
        public void SetQueryLevel12(Dictionary<string, string> valueUpdate)
        {
            QUERYSQL.valueUpdate = null; 
        }
        public QUERYSQL GetSQL()
        {

            return QUERYSQL;
        }

    }
    public class DELETEBUILDER:IQUERYSQL
    {
        QUERYSQL QUERYSQL = new QUERYSQL();
        public void SetQueryLevel1()
        {
            QUERYSQL.command.Add("DELETE ");
        }
        public void SetQueryLevel2(List<string> listSelect)
        {
            QUERYSQL.listSelect = null;
        }
        public void SetQueryLevel3()
        {
            QUERYSQL.command.Add("\nFROM ");
        }
        public void SetQueryLevel4(string tableName)
        {
            QUERYSQL.tableName = tableName;
        }
        public void SetQueryLevel5()
        {
            QUERYSQL.command.Add("\nWHERE ");
        }
        public void SetQueryLevel6(Dictionary<string, string> condition)
        {
            QUERYSQL.condition = condition;
        }
        public void SetQueryLevel7()
        {
            
        }

        public void SetQueryLevel8(string columGroupBy)
        {
       
        }

        public void SetQueryLevel9()
        {
            
        }
        public void SetQueryLevel10(string conditionHaving)
        {
       
        }
        public void SetQueryLevel11(Dictionary<string, string> valueInsert)
        {
            QUERYSQL.valueInsert = null;
        }
        public void SetQueryLevel12(Dictionary<string, string> valueUpdate)
        {
            QUERYSQL.valueUpdate = null; 
        }
        public QUERYSQL GetSQL()
        {

            return QUERYSQL;
        }
    }
    public class UPDATEBUILDER:IQUERYSQL
    {
        QUERYSQL QUERYSQL = new QUERYSQL();
        public void SetQueryLevel1()
        {
            QUERYSQL.command.Add("UPDATE ");
        }
        public void SetQueryLevel2(List<string> listSelect)
        {
            QUERYSQL.listSelect = null;
        }
        public void SetQueryLevel3()
        {
            QUERYSQL.command.Add("");
        }
        public void SetQueryLevel4(string tableName)
        {
            QUERYSQL.tableName = tableName;
        }
        public void SetQueryLevel5()
        {
            QUERYSQL.command.Add("\nWHERE ");
        }
        public void SetQueryLevel6(Dictionary<string, string> condition)
        {
            QUERYSQL.condition = condition;
        }
       
        public void SetQueryLevel7()
        {

        }

        public void SetQueryLevel8(string columGroupBy)
        {

        }

        public void SetQueryLevel9()
        {

        }
        public void SetQueryLevel10(string conditionHaving)
        {

        }
        public void SetQueryLevel11(Dictionary<string, string> valueInsert)
        {
            QUERYSQL.valueInsert = null;
        }
        public void SetQueryLevel12(Dictionary<string, string> valueUpdate)
        {
            QUERYSQL.valueUpdate = valueUpdate;
        }
        public QUERYSQL GetSQL()
        {

            return QUERYSQL;
        }

    }
    public class Director
    {
        private readonly IQUERYSQL objBuilder;
        public Director(IQUERYSQL builder)
        {
            objBuilder = builder;
        }
        public void CreateQuery(List<string> listSelect, string tableName, Dictionary<string, string> condition, string ColumnGroupby, string conditionHaving,Dictionary<string,string>valueInsert, Dictionary<string, string> valueUpdate)
        {
            objBuilder.SetQueryLevel1();
            objBuilder.SetQueryLevel2(listSelect);
            objBuilder.SetQueryLevel3();
            objBuilder.SetQueryLevel4(tableName);
            objBuilder.SetQueryLevel5();
            objBuilder.SetQueryLevel6(condition);
            objBuilder.SetQueryLevel7();
            objBuilder.SetQueryLevel8(ColumnGroupby);
            objBuilder.SetQueryLevel9();
            objBuilder.SetQueryLevel10(conditionHaving);
            objBuilder.SetQueryLevel11(valueInsert);
            objBuilder.SetQueryLevel12(valueUpdate);
        }
        public QUERYSQL GetSQL()
        {
            return objBuilder.GetSQL();
        }

       
    }

    class Program
    {
        static void Main(string[] args)
        {
            var Director1 = new Director(new SELECTBUILDER());
            List<string> listSelect=new List<string>();
            listSelect.Add("id");
            listSelect.Add("name");
            listSelect.Add("age");
            listSelect.Add("sex");
            string tablename= "information";
            Dictionary<string, string> condition= new Dictionary<string, string>();
            condition.Add("id","1");
            condition.Add("class", "12");
            string ColumnGroupby="name";
            string conditionHaving="sex =8";

            Director1.CreateQuery(listSelect, tablename, condition, ColumnGroupby, conditionHaving, condition,condition);
            var product = Director1.GetSQL();
            Console.WriteLine(product.QueryInsert());

            Console.WriteLine("----------------------------------------");
            var Director2 = new Director(new INSERTBUILDER());
            Director2.CreateQuery(listSelect, tablename, condition, ColumnGroupby, conditionHaving, condition, condition);
            var product2 = Director2.GetSQL();
            Console.WriteLine(product2.QueryInsert());

            Console.WriteLine("----------------------------------------");
            var Director3 = new Director(new UPDATEBUILDER());
            Director3.CreateQuery(listSelect, tablename, condition, ColumnGroupby, conditionHaving, condition, condition);
            var product3 = Director3.GetSQL();
            Console.WriteLine(product3.QueryInsert());

            Console.WriteLine("----------------------------------------");
            var Director4 = new Director(new DELETEBUILDER());
            Director4.CreateQuery(listSelect, tablename, condition, ColumnGroupby, conditionHaving, condition, condition);
            var product4 = Director4.GetSQL();
            Console.WriteLine(product4.QueryInsert());



            Console.ReadKey();
        }
    }
}
