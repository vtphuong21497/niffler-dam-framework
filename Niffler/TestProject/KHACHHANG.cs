﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class KHACHHANG
	{
		public KHACHHANG()
		{
		}

		public KHACHHANG(DataRow dr, NifflerContext context)
		{
			this._DATCHOs = null;
			this.GetDATCHOs = new Func<List<DATCHO>>(
				() => {
					return context.GetForeignKeyData<KHACHHANG,DATCHO>(this, "KHACHHANG", "DATCHO");
				}
			);							
			try { MaKH = (string)dr["MaKH"]; } catch { };
			try { Ten = (string)dr["Ten"]; } catch { };
			try { DChi = (string)dr["DChi"]; } catch { };
			try { DThoai = (string)dr["DThoai"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			KHACHHANG e = (KHACHHANG)obj;
			if (e == null) return false;
			return true && MaKH == e.MaKH;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaKH };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaKH { get; set; } 
		
		public string Ten { get; set; } 
		
		public string DChi { get; set; } 
		
		public string DThoai { get; set; } 
		
		private List<DATCHO> _DATCHOs;
		Func<List<DATCHO>> GetDATCHOs; 
		public List<DATCHO> DATCHOs 
		{
			get
			{
				if (_DATCHOs == null)
					_DATCHOs = GetDATCHOs();
				return _DATCHOs;
			}
		}
	}
}