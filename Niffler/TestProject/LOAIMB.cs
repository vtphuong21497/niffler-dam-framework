﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class LOAIMB
	{
		public LOAIMB()
		{
		}

		public LOAIMB(DataRow dr, NifflerContext context)
		{
			this._MAYBAYs = null;
			this.GetMAYBAYs = new Func<List<MAYBAY>>(
				() => {
					return context.GetForeignKeyData<LOAIMB,MAYBAY>(this, "LOAIMB", "MAYBAY");
				}
			);							
			this._KHANANGs = null;
			this.GetKHANANGs = new Func<List<KHANANG>>(
				() => {
					return context.GetForeignKeyData<LOAIMB,KHANANG>(this, "LOAIMB", "KHANANG");
				}
			);							
			try { MaLoai = (string)dr["MaLoai"]; } catch { };
			try { HangSX = (string)dr["HangSX"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			LOAIMB e = (LOAIMB)obj;
			if (e == null) return false;
			return true && MaLoai == e.MaLoai;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaLoai };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaLoai { get; set; } 
		
		public string HangSX { get; set; } 
		
		private List<MAYBAY> _MAYBAYs;
		Func<List<MAYBAY>> GetMAYBAYs; 
		public List<MAYBAY> MAYBAYs 
		{
			get
			{
				if (_MAYBAYs == null)
					_MAYBAYs = GetMAYBAYs();
				return _MAYBAYs;
			}
		}
		
		private List<KHANANG> _KHANANGs;
		Func<List<KHANANG>> GetKHANANGs; 
		public List<KHANANG> KHANANGs 
		{
			get
			{
				if (_KHANANGs == null)
					_KHANANGs = GetKHANANGs();
				return _KHANANGs;
			}
		}
	}
}