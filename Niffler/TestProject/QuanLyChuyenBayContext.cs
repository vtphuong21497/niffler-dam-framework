﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class QuanLyChuyenBayContext : NifflerContext
	{
		public QuanLyChuyenBayContext() :
			base(new SQLServerConnection("Data Source=DESKTOP-DJBU6V7\\SQLEXPRESS;Initial Catalog=QuanLyChuyenBay;Integrated Security=True"))
		{
			KHACHHANGs = new DbTable<KHACHHANG>(this);
			NHANVIENs = new DbTable<NHANVIEN>(this);
			LOAIMBs = new DbTable<LOAIMB>(this);
			MAYBAYs = new DbTable<MAYBAY>(this);
			CHUYENBAYs = new DbTable<CHUYENBAY>(this);
			LICHBAYs = new DbTable<LICHBAY>(this);
			DATCHOs = new DbTable<DATCHO>(this);
			KHANANGs = new DbTable<KHANANG>(this);
			PHANCONGs = new DbTable<PHANCONG>(this);
		}
		public virtual DbTable<KHACHHANG> KHACHHANGs { get; set; }
		public virtual DbTable<NHANVIEN> NHANVIENs { get; set; }
		public virtual DbTable<LOAIMB> LOAIMBs { get; set; }
		public virtual DbTable<MAYBAY> MAYBAYs { get; set; }
		public virtual DbTable<CHUYENBAY> CHUYENBAYs { get; set; }
		public virtual DbTable<LICHBAY> LICHBAYs { get; set; }
		public virtual DbTable<DATCHO> DATCHOs { get; set; }
		public virtual DbTable<KHANANG> KHANANGs { get; set; }
		public virtual DbTable<PHANCONG> PHANCONGs { get; set; }
	}
}