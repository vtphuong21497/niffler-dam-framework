﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class PHANCONG
	{
		public PHANCONG()
		{
		}

		public PHANCONG(DataRow dr, NifflerContext context)
		{
			this._NHANVIEN = null;
			this.GetNHANVIEN = new Func<NHANVIEN>(
				() => {
					return context.GetForeignKeyData<PHANCONG,NHANVIEN>(this, "PHANCONG", "NHANVIEN")[0];
				}
			);
			this._LICHBAY = null;
			this.GetLICHBAY = new Func<LICHBAY>(
				() => {
					return context.GetForeignKeyData<PHANCONG,LICHBAY>(this, "PHANCONG", "LICHBAY")[0];
				}
			);
			try { MaNV = (string)dr["MaNV"]; } catch { };
			try { NgayDi = (DateTime)dr["NgayDi"]; } catch { };
			try { MaCB = (string)dr["MaCB"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			PHANCONG e = (PHANCONG)obj;
			if (e == null) return false;
			return true && MaNV == e.MaNV && NgayDi == e.NgayDi && MaCB == e.MaCB;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaNV, NgayDi, MaCB };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaNV { get; set; } 
		
		public DateTime NgayDi { get; set; } 
		
		public string MaCB { get; set; } 

		private NHANVIEN _NHANVIEN;
		Func<NHANVIEN> GetNHANVIEN; 
		public NHANVIEN NHANVIEN 
		{
			get
			{
				if (_NHANVIEN == null)
					_NHANVIEN = GetNHANVIEN();
				return _NHANVIEN;
			}
		}

		private LICHBAY _LICHBAY;
		Func<LICHBAY> GetLICHBAY; 
		public LICHBAY LICHBAY 
		{
			get
			{
				if (_LICHBAY == null)
					_LICHBAY = GetLICHBAY();
				return _LICHBAY;
			}
		}
	}
}