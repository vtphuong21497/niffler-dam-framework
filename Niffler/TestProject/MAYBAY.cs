﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class MAYBAY
	{
		public MAYBAY()
		{
		}

		public MAYBAY(DataRow dr, NifflerContext context)
		{
			this._LOAIMB = null;
			this.GetLOAIMB = new Func<LOAIMB>(
				() => {
					return context.GetForeignKeyData<MAYBAY,LOAIMB>(this, "MAYBAY", "LOAIMB")[0];
				}
			);
			this._LICHBAYs = null;
			this.GetLICHBAYs = new Func<List<LICHBAY>>(
				() => {
					return context.GetForeignKeyData<MAYBAY,LICHBAY>(this, "MAYBAY", "LICHBAY");
				}
			);							
			try { SoHieu = (int)dr["SoHieu"]; } catch { };
			try { MaLoai = (string)dr["MaLoai"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			MAYBAY e = (MAYBAY)obj;
			if (e == null) return false;
			return true && SoHieu == e.SoHieu && MaLoai == e.MaLoai;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, SoHieu, MaLoai };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public int SoHieu { get; set; } 
		
		public string MaLoai { get; set; } 

		private LOAIMB _LOAIMB;
		Func<LOAIMB> GetLOAIMB; 
		public LOAIMB LOAIMB 
		{
			get
			{
				if (_LOAIMB == null)
					_LOAIMB = GetLOAIMB();
				return _LOAIMB;
			}
		}
		
		private List<LICHBAY> _LICHBAYs;
		Func<List<LICHBAY>> GetLICHBAYs; 
		public List<LICHBAY> LICHBAYs 
		{
			get
			{
				if (_LICHBAYs == null)
					_LICHBAYs = GetLICHBAYs();
				return _LICHBAYs;
			}
		}
	}
}