﻿using NifflerConnection.Connection;
using NifflerConnection.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NifflerConnection;
using System.Dynamic;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var db = new QuanLyChuyenBayContext();

            // Test 01 - Insert
            KHACHHANG kh = new KHACHHANG() { MaKH = "123123123", Ten = "Đông Võ" };
            db.KHACHHANGs.Add(kh);
            db.SaveChanges();
            Console.WriteLine("Test 01 finished - inserted");
            Console.ReadLine();

            // Test 02 - Update
            kh.DChi = "Đắk Lắk";
            db.KHACHHANGs.Update(kh);
            db.SaveChanges();
            Console.WriteLine("Test 02 finished - updated");
            Console.ReadLine();

            // Test 03 - Delete
            db.KHACHHANGs.Delete(kh);
            db.SaveChanges();
            Console.WriteLine("Test 03 finished - deleted");
            Console.ReadLine();

            // Test 04 - select *
            string query = db.GetQueryable(db.KHACHHANGs).ToString();
            Console.WriteLine(query);
            Console.ReadLine();
            var kh_data = db.GetQueryable(db.KHACHHANGs).All();
            foreach (KHACHHANG tmp in kh_data)
            {
                Console.WriteLine("MaKH = {0}", tmp.MaKH);
                Console.WriteLine("Ten = {0}", tmp.Ten);
                Console.WriteLine("DChi = {0}", tmp.DChi);
                Console.WriteLine("DThoai = {0}", tmp.DThoai);
            }
            Console.WriteLine("Test 04 finished - selected");
            Console.ReadLine();

            // Test 05 - where
            Console.WriteLine(db.GetQueryable(db.KHACHHANGs).Where(p => p.MaKH == "0009").ToString());
            Console.ReadLine();
            var kh0009 = db.GetQueryable(db.KHACHHANGs).Where(p => p.MaKH == "0009").Single();
            Console.WriteLine(kh0009.MaKH);
            var datCho = kh0009.DATCHOs;
            foreach (var p in datCho)
            {
                Console.WriteLine("MaKH = {0}", p.MaKH);
                Console.WriteLine("MaCB = {0}", p.MaCB);
                Console.WriteLine("NgayDi = {0}", p.NgayDi);
            }

            Console.WriteLine("Test 05 finished - selected");
            Console.ReadLine();

            // Test 06 - Join
            var query06 = db.GetQueryable(db.NHANVIENs).Join(db.PHANCONGs,
                nv => new { nv.MaNV },
                pc => new { pc.MaNV },
                (nv, pc) => new { NhanVien = nv, PhanCong = pc });
            Console.WriteLine(query06.ToString());
            Console.ReadLine();

            var test06 = query06.Single();

            Console.WriteLine("NhanVien.MaNV = {0}", test06.NhanVien.MaNV);
            Console.WriteLine("PhanCong.MaNV = {0}", test06.PhanCong.MaNV);
            Console.WriteLine("Ten = {0}", test06.NhanVien.Ten);
            Console.WriteLine("Luong = {0}", test06.NhanVien.Luong);

            Console.WriteLine("Test 06 finished - selected");
            Console.ReadLine();

            // Test 07 - Join + Where     
            test06 = query06.Where(e => e.NhanVien.MaNV == "1001").Single();

            Console.WriteLine("NhanVien.MaNV = {0}", test06.NhanVien.MaNV);
            Console.WriteLine("PhanCong.MaNV = {0}", test06.PhanCong.MaNV);
            Console.WriteLine("Ten = {0}", test06.NhanVien.Ten);
            Console.WriteLine("Luong = {0}", test06.NhanVien.Luong);

            Console.WriteLine("Test 07 finished - selected");
            Console.ReadLine();

            // Test 08 - Group by MAYBAY theo mã loại
            var query08 = db.GetQueryable(db.MAYBAYs).GroupBy(e => new { e.MaLoai });
            Console.WriteLine(query08.ToString());
            Console.ReadLine();

            var test08 = query08.All();
            foreach (var tmp in test08)
            {
                Console.WriteLine(tmp.MaLoai);
            }
            Console.WriteLine("Test 08 finished - selected");
            Console.ReadLine();

            // Test 09 - Group by số chuyễn bay theo loại máy bay
            var query09 = db.GetQueryable(db.LICHBAYs)
                .GroupBy(e => new { e.MaLoai, SoLuong = NifflerFunc.COUNT(e.MaLoai) });

            Console.WriteLine(query09.ToString());
            Console.ReadLine();

            var test09 = query09.All();
            foreach (var tmp in test09)
            {
                Console.WriteLine("{0} {1}", tmp.MaLoai, tmp.SoLuong);
            }
            Console.WriteLine("Test 09 finished");
            Console.ReadLine();

            // Test 10 - Group by có Having
            var query10 = db.GetQueryable(db.LICHBAYs)
                .GroupBy(e => new { e.MaLoai, SoLuong = NifflerFunc.COUNT(e.MaLoai) },
                            e => NifflerFunc.COUNT(e.MaLoai) > 2);

            Console.WriteLine(query10.ToString());
            Console.ReadLine();

            var test10 = query10.All();
            foreach (var tmp in test10)
            {
                Console.WriteLine("{0} {1}", tmp.MaLoai, tmp.SoLuong);
            }
            Console.WriteLine("Test 10 finished");
            Console.ReadLine();
        }
    }
}
