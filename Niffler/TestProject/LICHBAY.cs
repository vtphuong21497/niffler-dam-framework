﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class LICHBAY
	{
		public LICHBAY()
		{
		}

		public LICHBAY(DataRow dr, NifflerContext context)
		{
			this._MAYBAY = null;
			this.GetMAYBAY = new Func<MAYBAY>(
				() => {
					return context.GetForeignKeyData<LICHBAY,MAYBAY>(this, "LICHBAY", "MAYBAY")[0];
				}
			);
			this._CHUYENBAY = null;
			this.GetCHUYENBAY = new Func<CHUYENBAY>(
				() => {
					return context.GetForeignKeyData<LICHBAY,CHUYENBAY>(this, "LICHBAY", "CHUYENBAY")[0];
				}
			);
			this._DATCHOs = null;
			this.GetDATCHOs = new Func<List<DATCHO>>(
				() => {
					return context.GetForeignKeyData<LICHBAY,DATCHO>(this, "LICHBAY", "DATCHO");
				}
			);							
			this._PHANCONGs = null;
			this.GetPHANCONGs = new Func<List<PHANCONG>>(
				() => {
					return context.GetForeignKeyData<LICHBAY,PHANCONG>(this, "LICHBAY", "PHANCONG");
				}
			);							
			try { NgayDi = (DateTime)dr["NgayDi"]; } catch { };
			try { MaCB = (string)dr["MaCB"]; } catch { };
			try { SoHieu = (Nullable<int>)dr["SoHieu"]; } catch { };
			try { MaLoai = (string)dr["MaLoai"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			LICHBAY e = (LICHBAY)obj;
			if (e == null) return false;
			return true && NgayDi == e.NgayDi && MaCB == e.MaCB;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, NgayDi, MaCB };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public DateTime NgayDi { get; set; } 
		
		public string MaCB { get; set; } 
		
		public Nullable<int> SoHieu { get; set; } 
		
		public string MaLoai { get; set; } 

		private MAYBAY _MAYBAY;
		Func<MAYBAY> GetMAYBAY; 
		public MAYBAY MAYBAY 
		{
			get
			{
				if (_MAYBAY == null)
					_MAYBAY = GetMAYBAY();
				return _MAYBAY;
			}
		}

		private CHUYENBAY _CHUYENBAY;
		Func<CHUYENBAY> GetCHUYENBAY; 
		public CHUYENBAY CHUYENBAY 
		{
			get
			{
				if (_CHUYENBAY == null)
					_CHUYENBAY = GetCHUYENBAY();
				return _CHUYENBAY;
			}
		}
		
		private List<DATCHO> _DATCHOs;
		Func<List<DATCHO>> GetDATCHOs; 
		public List<DATCHO> DATCHOs 
		{
			get
			{
				if (_DATCHOs == null)
					_DATCHOs = GetDATCHOs();
				return _DATCHOs;
			}
		}
		
		private List<PHANCONG> _PHANCONGs;
		Func<List<PHANCONG>> GetPHANCONGs; 
		public List<PHANCONG> PHANCONGs 
		{
			get
			{
				if (_PHANCONGs == null)
					_PHANCONGs = GetPHANCONGs();
				return _PHANCONGs;
			}
		}
	}
}