﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class CHUYENBAY
	{
		public CHUYENBAY()
		{
		}

		public CHUYENBAY(DataRow dr, NifflerContext context)
		{
			this._LICHBAYs = null;
			this.GetLICHBAYs = new Func<List<LICHBAY>>(
				() => {
					return context.GetForeignKeyData<CHUYENBAY,LICHBAY>(this, "CHUYENBAY", "LICHBAY");
				}
			);							
			try { MaCB = (string)dr["MaCB"]; } catch { };
			try { SBDi = (string)dr["SBDi"]; } catch { };
			try { SBDe = (string)dr["SBDe"]; } catch { };
			try { GioDi = (TimeSpan)dr["GioDi"]; } catch { };
			try { GioDen = (TimeSpan)dr["GioDen"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			CHUYENBAY e = (CHUYENBAY)obj;
			if (e == null) return false;
			return true && MaCB == e.MaCB;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaCB };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaCB { get; set; } 
		
		public string SBDi { get; set; } 
		
		public string SBDe { get; set; } 
		
		public TimeSpan GioDi { get; set; } 
		
		public TimeSpan GioDen { get; set; } 
		
		private List<LICHBAY> _LICHBAYs;
		Func<List<LICHBAY>> GetLICHBAYs; 
		public List<LICHBAY> LICHBAYs 
		{
			get
			{
				if (_LICHBAYs == null)
					_LICHBAYs = GetLICHBAYs();
				return _LICHBAYs;
			}
		}
	}
}