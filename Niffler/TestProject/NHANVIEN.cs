﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class NHANVIEN
	{
		public NHANVIEN()
		{
		}

		public NHANVIEN(DataRow dr, NifflerContext context)
		{
			this._KHANANGs = null;
			this.GetKHANANGs = new Func<List<KHANANG>>(
				() => {
					return context.GetForeignKeyData<NHANVIEN,KHANANG>(this, "NHANVIEN", "KHANANG");
				}
			);							
			this._PHANCONGs = null;
			this.GetPHANCONGs = new Func<List<PHANCONG>>(
				() => {
					return context.GetForeignKeyData<NHANVIEN,PHANCONG>(this, "NHANVIEN", "PHANCONG");
				}
			);							
			try { MaNV = (string)dr["MaNV"]; } catch { };
			try { Ten = (string)dr["Ten"]; } catch { };
			try { DChi = (string)dr["DChi"]; } catch { };
			try { DThoai = (string)dr["DThoai"]; } catch { };
			try { Luong = (double)dr["Luong"]; } catch { };
			try { LoaiNV = (bool)dr["LoaiNV"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			NHANVIEN e = (NHANVIEN)obj;
			if (e == null) return false;
			return true && MaNV == e.MaNV;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaNV };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaNV { get; set; } 
		
		public string Ten { get; set; } 
		
		public string DChi { get; set; } 
		
		public string DThoai { get; set; } 
		
		public double Luong { get; set; } 
		
		public bool LoaiNV { get; set; } 
		
		private List<KHANANG> _KHANANGs;
		Func<List<KHANANG>> GetKHANANGs; 
		public List<KHANANG> KHANANGs 
		{
			get
			{
				if (_KHANANGs == null)
					_KHANANGs = GetKHANANGs();
				return _KHANANGs;
			}
		}
		
		private List<PHANCONG> _PHANCONGs;
		Func<List<PHANCONG>> GetPHANCONGs; 
		public List<PHANCONG> PHANCONGs 
		{
			get
			{
				if (_PHANCONGs == null)
					_PHANCONGs = GetPHANCONGs();
				return _PHANCONGs;
			}
		}
	}
}