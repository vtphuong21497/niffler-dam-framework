﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class KHANANG
	{
		public KHANANG()
		{
		}

		public KHANANG(DataRow dr, NifflerContext context)
		{
			this._NHANVIEN = null;
			this.GetNHANVIEN = new Func<NHANVIEN>(
				() => {
					return context.GetForeignKeyData<KHANANG,NHANVIEN>(this, "KHANANG", "NHANVIEN")[0];
				}
			);
			this._LOAIMB = null;
			this.GetLOAIMB = new Func<LOAIMB>(
				() => {
					return context.GetForeignKeyData<KHANANG,LOAIMB>(this, "KHANANG", "LOAIMB")[0];
				}
			);
			try { MaNV = (string)dr["MaNV"]; } catch { };
			try { MaLoai = (string)dr["MaLoai"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			KHANANG e = (KHANANG)obj;
			if (e == null) return false;
			return true && MaNV == e.MaNV && MaLoai == e.MaLoai;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaNV, MaLoai };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaNV { get; set; } 
		
		public string MaLoai { get; set; } 

		private NHANVIEN _NHANVIEN;
		Func<NHANVIEN> GetNHANVIEN; 
		public NHANVIEN NHANVIEN 
		{
			get
			{
				if (_NHANVIEN == null)
					_NHANVIEN = GetNHANVIEN();
				return _NHANVIEN;
			}
		}

		private LOAIMB _LOAIMB;
		Func<LOAIMB> GetLOAIMB; 
		public LOAIMB LOAIMB 
		{
			get
			{
				if (_LOAIMB == null)
					_LOAIMB = GetLOAIMB();
				return _LOAIMB;
			}
		}
	}
}