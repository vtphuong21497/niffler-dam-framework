﻿namespace TestProject
{
	using System;
    using System.Collections.Generic;
	using System.Data;
	using NifflerConnection.Connection;

	public class DATCHO
	{
		public DATCHO()
		{
		}

		public DATCHO(DataRow dr, NifflerContext context)
		{
			this._KHACHHANG = null;
			this.GetKHACHHANG = new Func<KHACHHANG>(
				() => {
					return context.GetForeignKeyData<DATCHO,KHACHHANG>(this, "DATCHO", "KHACHHANG")[0];
				}
			);
			this._LICHBAY = null;
			this.GetLICHBAY = new Func<LICHBAY>(
				() => {
					return context.GetForeignKeyData<DATCHO,LICHBAY>(this, "DATCHO", "LICHBAY")[0];
				}
			);
			try { MaKH = (string)dr["MaKH"]; } catch { };
			try { NgayDi = (DateTime)dr["NgayDi"]; } catch { };
			try { MaCB = (string)dr["MaCB"]; } catch { };
			
		}

		public override bool Equals(object obj)
		{
			DATCHO e = (DATCHO)obj;
			if (e == null) return false;
			return true && MaKH == e.MaKH && NgayDi == e.NgayDi && MaCB == e.MaCB;
		}

		public override int GetHashCode()
		{
			object[] objs = { null, MaKH, NgayDi, MaCB };			
			unchecked
			{				
				int result = 7;
				foreach (var obj in objs)
				{
					result += result * 17 + ((obj == null) ? 0 : obj.GetHashCode());   
				}
				return result;
			}
		}

			
		public string MaKH { get; set; } 
		
		public DateTime NgayDi { get; set; } 
		
		public string MaCB { get; set; } 

		private KHACHHANG _KHACHHANG;
		Func<KHACHHANG> GetKHACHHANG; 
		public KHACHHANG KHACHHANG 
		{
			get
			{
				if (_KHACHHANG == null)
					_KHACHHANG = GetKHACHHANG();
				return _KHACHHANG;
			}
		}

		private LICHBAY _LICHBAY;
		Func<LICHBAY> GetLICHBAY; 
		public LICHBAY LICHBAY 
		{
			get
			{
				if (_LICHBAY == null)
					_LICHBAY = GetLICHBAY();
				return _LICHBAY;
			}
		}
	}
}