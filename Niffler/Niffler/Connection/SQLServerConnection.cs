﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public class SQLServerConnection : RDBMSConnection
    {
        public SQLServerConnection(string connectionString) : 
            base(connectionString)
        {
        }

        protected override RDBMSCommand GetCommand()
        {
            return new SQLServerCommand();
        }

        protected override DbConnection GetConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        public override string ClassName()
        {
            return "SQLServerConnection";
        }
    }
}
