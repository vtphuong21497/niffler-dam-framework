﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public class NifflerQueryable<T> where T : class
    {
        public NifflerContext context;
        int TakeNumber = -1;
        List<string> SelectContent;
        Dictionary<string, string> FromContent;
        Dictionary<string, string> JoinContent;
        string WhereContent;
        string HavingContent;
        List<string> GroupByContent;

        public NifflerQueryable()
        {
            SelectContent = new List<string>();
            FromContent = new Dictionary<string, string>();
            JoinContent = new Dictionary<string, string>();
            FromContent.Add(typeof(T).Name, "[Niffler1]");
            WhereContent = "";
            HavingContent = "";
            GroupByContent = new List<string>();
        }

        private Type FindType(Type root)
        {
            if (context.ContainsTable(root.Name))
                return root;

            foreach (var info in root.GetProperties())
            {
                Type type = FindType(info.PropertyType);
                if (context.ContainsTable(type.Name))
                    return type;
            }
            return null;
        }

        private string NormalizeExpressionBody(string body, string paramName)
        {
            int cur = body.IndexOf(paramName);
            List<int> startIndex = new List<int>();
            List<int> endIndex = new List<int>();
            while (cur != -1)
            {
                int p = body.IndexOf(' ', cur + 1);
                if (p == -1)
                    p = body.IndexOf(')', cur + 1);
                if (p == -1)
                    p = body.Length - 1;
                int q = p;
                while (cur < body.Length && body[cur] != '.')
                    cur++;
                while (p >= 0 && body[p] != '.')
                    p--;
                if (cur > p)
                    break;
                startIndex.Add(cur);
                endIndex.Add(p);
                cur = body.IndexOf(paramName, q);
            }
            for (int i = startIndex.Count - 1; i >= 0; i--)
                body = body.Remove(startIndex[i], endIndex[i] - startIndex[i]);
            return body;
        }

        public string ConditionClause(Expression<Func<T,bool>> e)
        {
            string expBody = NormalizeExpressionBody(((LambdaExpression)e).Body.ToString(), e.Parameters[0].Name);
            // Gives: ((x.Id > 5) AndAlso (x.Warranty != False))
            var paramName = e.Parameters[0].Name;
            Type type = FindType(e.Parameters[0].Type);
            var paramTypeName = FromContent[type.Name];

            // You could easily add "OrElse" and others...
            string result = expBody.Replace(paramName + ".", paramTypeName + ".")
                             .Replace("AndAlso", "And")
                             .Replace("OrElse", "Or")
                             .Replace("==", "=");

            int count = 0;
            int start = 0;
            while ((start = result.IndexOf("\"", start)) != -1)
            {
                if (count % 2 == 0)
                    result = result.Insert(start, "N");
                start += 2;
                count++;
            }

            result = result.Replace("\"", "'");

            return result;
        }

        public NifflerQueryable<T> Where(Expression<Func<T,bool>> e) 
        {
            WhereContent = ConditionClause(e);
            return this;
        }

        public NifflerQueryable<Tres> From<Tnew,Tres>(DbTable<Tnew> table, Expression<Func<T,Tnew,Tres>> e) where Tnew : class where Tres:class
        {
            AddToFrom(typeof(Tnew));
            var x = this.Clone<Tres>();
            return x;
        }

        public NifflerQueryable<Tres> Select<Tres>(Expression<Func<T,Tres>> e) where Tres : class
        {
            SelectContent.Clear();
            var exp = (NewExpression)e.Body;
            foreach (var tmp in exp.Arguments)
            {
                ParameterExpression param = null;
                for (int i = 0; i < e.Parameters.Count; i++)
                    if (tmp.ToString().StartsWith(e.Parameters[i].Name))
                        param = e.Parameters[i];

                string p = NormalizeExpressionBody(tmp.ToString(), param.Name);
                Type type = FindType(param.Type);
                if (tmp.ToString().IndexOf('.') == -1)
                    p += ".*";
                SelectContent.Add(p.Replace(param.Name, FromContent[type.Name]));
            }
            var x = this.Clone<Tres>();
            return x;
        }

        protected void AddToFrom(Type type)
        {
            FromContent.Add(type.Name, String.Format("[Niffler{0}]", FromContent.Count + 1));
        }

        public NifflerQueryable<Tres> Join<Tjoin,Tkey,Tres>(
            DbTable<Tjoin> db, 
            Expression<Func<T,Tkey>> key,
            Expression<Func<Tjoin,Tkey>> keyJoin,
            Expression<Func<T,Tjoin,Tres>> e)
                        where Tres : class where Tjoin : class
        {
            AddToFrom(typeof(Tjoin));

            string onClause = "";

            var eKey = (NewExpression)key.Body;
            var eKeyJoin = (NewExpression)keyJoin.Body;

            var eKeyList = eKey.Arguments.ToList();
            var eKeyJoinList = eKeyJoin.Arguments.ToList();

            for (int i = 0; i < eKeyList.Count; i++)
            {
                string tmp = NormalizeExpressionBody(eKeyList[i].ToString().Replace(".ToString()", ""), key.Parameters[0].Name);
                Type type = FindType(key.Parameters[0].Type);
                string valKey = tmp.Replace(key.Parameters[0].Name, FromContent[type.Name]).Replace(".ToString()","");

                tmp = NormalizeExpressionBody(eKeyJoinList[i].ToString().Replace(".ToString()", ""), keyJoin.Parameters[0].Name);
                type = FindType(keyJoin.Parameters[0].Type);
                string valKeyJoin = tmp.Replace(keyJoin.Parameters[0].Name, FromContent[keyJoin.Parameters[0].Type.Name]).Replace(".ToString()", "");

                onClause += valKey + " = " + valKeyJoin;
                if (i < eKeyList.Count - 1)
                    onClause += " AND ";
            }

            SelectContent.Clear();
            onClause = "(" + onClause + ")";
            JoinContent.Add(typeof(Tjoin).Name, onClause);

            var exp = (NewExpression)e.Body;
            foreach (var tmp in exp.Arguments)
            {
                ParameterExpression param = null;
                for (int i = 0; i < e.Parameters.Count; i++)
                    if (tmp.ToString().StartsWith(e.Parameters[i].Name))
                        param = e.Parameters[i];

                string p = NormalizeExpressionBody(tmp.ToString(), param.Name);
                Type type = FindType(param.Type);
                if (tmp.ToString().IndexOf('.') == -1)
                    p += ".*";
                SelectContent.Add(p.Replace(param.Name, FromContent[type.Name]));
            }
            
            var x = this.Clone<Tres>();
            return x;
        }

        private bool IsNotFunc(string s)
        {
            return !(s.IndexOf('(') != -1 && s.IndexOf(')') != -1);
        }

        public NifflerQueryable<TResult> GroupBy<TResult>(Expression<Func<T, TResult>> e, Expression<Func<T, bool>> havingE = null) where TResult : class
        {
            SelectContent.Clear();
            var newExp = (NewExpression)e.Body;
            foreach (var k in newExp.Arguments)
            {
                string body = NormalizeExpressionBody(k.ToString(), e.Parameters[0].Name);
                Type type = FindType(e.Parameters[0].Type);
                string val = body
                    .Replace(e.Parameters[0].Name, FromContent[type.Name]);
                SelectContent.Add(val);
                if (IsNotFunc(val))
                    GroupByContent.Add(val);
            }           
            
            if (havingE != null)
            {
                HavingContent = ConditionClause(havingE);
            }

            NifflerQueryable<TResult> x = Clone<TResult>();
            return x;
        }

        public NifflerQueryable<Tout> Clone<Tout>() where Tout: class
        {
            NifflerQueryable<Tout> result = new NifflerQueryable<Tout>();
            result.context = this.context;
            result.FromContent = FromContent;
            result.SelectContent = SelectContent;
            result.WhereContent = WhereContent;
            result.TakeNumber = TakeNumber;
            result.GroupByContent = GroupByContent;
            result.HavingContent = HavingContent;
            result.JoinContent = JoinContent;
            return result;
        }

        protected string BuildSelectContent()
        {
            var result = "SELECT ";
            result += ((TakeNumber > 0) ? string.Format("TOP {0} ", TakeNumber) : "");

            if (SelectContent.Count == 0)
                return result += "*";
            
            for (int i = 0; i < SelectContent.Count; i++)
            {
                result += SelectContent[i];
                if (i < SelectContent.Count - 1)
                    result += ",";
            }
            return result;
        }

        protected string BuildFromContent()
        {
            var keys = FromContent.Keys;
            var result = " FROM ";

            int count = 0;
            for (int i = 0; i < keys.Count; i++)
                if (!JoinContent.ContainsKey(keys.ElementAt(i)))
                    count++;

            for (int i = 0; i < keys.Count; i++)
            {
                string key = keys.ElementAt(i);
                if (JoinContent.ContainsKey(key))
                    continue;
                result += key + " as " + FromContent[key];
                if (i < count - 1)
                    result += ", ";
            }

            for (int i = 0; i < keys.Count; i++)
            {
                string key = keys.ElementAt(i);
                if (!JoinContent.ContainsKey(key))
                    continue;
                result += " JOIN " + key + " as " + FromContent[key] + " ON " + JoinContent[key];
            }
            return result;
        }

        protected string BuildWhereContent()
        {
            if (WhereContent.Length > 0)
                return " WHERE " + WhereContent;
            return "";
        }

        protected string BuildGroupByContent()
        {
            string str = "";
            for (int i = 0; i < GroupByContent.Count; i++)
            {
                str += GroupByContent[i];
                if (i < GroupByContent.Count - 1)
                    str += ", ";
            }
            if (GroupByContent.Count > 0)
                return " GROUP BY " + str;
            return "";
        }

        protected string BuildHavingContent()
        {
            if (HavingContent.Length > 0)
                return " HAVING " + HavingContent;
            return "";
        }

        protected string BuildOrderContent()
        {
            return "";
        }

        protected string Build()
        {
            string result = BuildSelectContent()
                + BuildFromContent()
                + BuildWhereContent()
                + BuildGroupByContent()
                + BuildHavingContent()
                + BuildOrderContent();
            return result;
        }

        public override string ToString()
        {
            return Build();
        }

        public List<T> Take(int number)
        {
            TakeNumber = number;
            return context.ExecuteQuery<T>(Build());
        }

        public List<T> All()
        {
            TakeNumber = -1;
            return context.ExecuteQuery<T>(Build()); 
        }

        public T Single()
        {
            TakeNumber = 1;
            return context.ExecuteQuery<T>(Build())[0];
        }
    }
}
