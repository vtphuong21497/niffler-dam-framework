﻿using NifflerConnection.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public class DbRecord
    {
        protected object Element;
        protected DbAction Action;
        public DbRecord(object element, DbAction action)
        {
            Element = element;
            Action = action;
        }

        public int Execute(RDBMSConnection connection, Table table)
        {
            return Action.Execute(Element, connection, table);
        }
    }
}
