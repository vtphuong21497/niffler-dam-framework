﻿using NifflerConnection.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public abstract class DbAction
    {
        public abstract int Execute(object e, RDBMSConnection connection, Table table);
    }

    public class DbUpdate : DbAction
    {
        public override int Execute(object e, RDBMSConnection connection, Table table)
        {
            List<string> attrName = new List<string>();
            List<object> attrValue = new List<object>();
            List<string> pkName = new List<string>();
            List<object> pkValue = new List<object>();

            foreach (var attr in table.GetAttributes())
            {
                object obj = e.GetType().GetProperty(attr.Name).GetValue(e);
                if (obj != null)
                {
                    if (attr.IsPrimaryKey)
                    {
                        pkName.Add(attr.Name);
                        pkValue.Add(obj);
                    }
                    else {
                        attrName.Add(attr.Name);
                        attrValue.Add(obj);
                    }
                }
            }

            string sqlUpdate = connection.Command.GetUpdateCommand(table.Name, pkName, pkValue, attrName, attrValue);
            return connection.ExecuteNonQuery(sqlUpdate);
        }
    }

    public class DbInsert : DbAction
    {
        public override int Execute(object e, RDBMSConnection connection, Table table)
        {
            List<string> attrName = new List<string>();
            List<object> attrValue = new List<object>();

            foreach (var attr in table.GetAttributes())
            {
                object obj = e.GetType().GetProperty(attr.Name).GetValue(e);
                if (obj != null)
                {
                    attrName.Add(attr.Name);
                    attrValue.Add(obj);
                }
            }

            string sqlInsert = connection.Command.GetInsertCommand(table.Name, attrName, attrValue);
            return connection.ExecuteNonQuery(sqlInsert);
        }
    }

    public class DbDelete : DbAction
    {
        public override int Execute(object e, RDBMSConnection connection, Table table)
        {
            List<string> pkName = new List<string>();
            List<object> pkValue = new List<object>();

            foreach (var attr in table.GetAttributes())
            {
                object obj = e.GetType().GetProperty(attr.Name).GetValue(e);
                if (obj != null)
                {
                    pkName.Add(attr.Name);
                    pkValue.Add(obj);
                }
            }

            string sqlDelete = connection.Command.GetDeleteCommand(table.Name, pkName, pkValue);
            return connection.ExecuteNonQuery(sqlDelete);
        }
    }
}
