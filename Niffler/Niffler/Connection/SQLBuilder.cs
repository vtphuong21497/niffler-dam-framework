﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public class SQLBuilder
    {
        private RDBMSCommand Command;

        private string FromContent;
        private string WhereContent;
        private string SelectContent;
        private List<string> JoinContents;
        private string HavingContent;
        private string GroupByContent;
        
        public SQLBuilder(RDBMSCommand command)
        {
            Command = command;
        }

        public SQLBuilder From(string tableName, string simple)
        {
            FromContent = tableName + " as " + simple;
            return this;
        }

        public SQLBuilder From(DbTable table, string simple)
        {
            FromContent = table.GetTypeDescription() + " as " + simple;
            return this;
        }

        public SQLBuilder Join(DbTable table, string simple, string on)
        {
            return this;
        }

        public SQLBuilder Where(string content, params object[] args)
        {
            for (int i = 1; i <= args.Length; i++)
            {
                string p = Command.ToString(args[i - 1]);
                content = content.Replace("@" + i.ToString(), Command.ToString(args[i - 1]));
            }
            WhereContent = content;
            return this;
        }

        public SQLBuilder Where<T>(Expression<Func<T, bool>> expression)
        {
            return this;
        }

        public SQLBuilder Select(string content)
        {
            SelectContent = content;
            return this;
        }

        public SQLBuilder Having(string content)
        {
            return this;
        }

        public SQLBuilder GroupBy(string content)
        {
            return this;
        }

        public string Build()
        {
            return "SELECT " + SelectContent
                + "\nFROM " + FromContent
                + (!string.IsNullOrEmpty(WhereContent) ? "\nWHERE " + WhereContent : "")
                + (!string.IsNullOrEmpty(HavingContent) ? "\nHAVING " + WhereContent : "")
                + (!string.IsNullOrEmpty(GroupByContent) ? "\nGROUP BY " + WhereContent : "");
        }
    }
}
