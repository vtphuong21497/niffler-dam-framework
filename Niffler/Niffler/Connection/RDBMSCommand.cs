﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public abstract class RDBMSCommand
    {
        public abstract string ToString(object obj);
        // command returns value to get datatable with format (Name-Table(F)-Column(F)-Table-Column)
        public abstract string GetRetrieveForeignKeyCommand();
        // command returns value to get datatable with format (Table-Column)
        public abstract string GetRetrievePrimaryKeyCommand();
        // command returns value to select top 0
        public abstract string GetRetrieveAttributeOfTableCommand(string tableName);
        // command returns value to get database name
        public abstract string GetRetrieveDatabaseNameCommand();

        public string ConcatEquals(List<string> attrLeft, List<string> attrRight, string delimeter)
        {
            int count = attrLeft.Count;
            string result = "";
            for (int i = 0; i < count; i++)
            {
                result += attrLeft[i] + "=" + attrRight[i];
                if (i < count - 1)
                {
                    result += delimeter;
                }
            }
            return result;
        }

        public string ConcatEquals(List<string> attributeName, List<object> attributeValue, string delimeter)
        {
            int count = attributeName.Count;
            string result = "";
            for (int i = 0; i < count; i++)
            {
                result += attributeName[i] + "=" + ToString(attributeValue[i]);
                if (i < count - 1)
                {
                    result += delimeter;
                }
            }
            return result;
        }

        public string ConcatWhere(List<string> attributeName, List<object> attributeValue)
        {
            return ConcatEquals(attributeName, attributeValue, " AND ");
        }

        public string ConcatName(List<string> attributeName)
        {
            int count = attributeName.Count;
            string title = "";
            for (int i = 0; i < count; i++)
            {
                title += attributeName[i];
                if (i < count - 1)
                {
                    title += ',';
                }
            }
            return title;
        }

        public string ConcatValue(List<object> attributeValue)
        {
            int count = attributeValue.Count;
            string value = "";
            for (int i = 0; i < count; i++)
            {
                value += ToString(attributeValue[i]);
                if (i < count - 1)
                {
                    value += ',';
                }
            }
            return value;
        }

        public abstract string GetInsertCommand(string tableName, List<string> attributeName, List<object> attributeValue);
        public abstract string GetUpdateCommand(string tableName, List<string> pkName, List<object> pkValue, List<string> attrName, List<object> attrValue);
        public abstract string GetDeleteCommand(string tableName, List<string> pkName, List<object> pkValue);

        public abstract string GetJoinCommand(string leftName, string rightName, List<string> attrLeft, List<string> attrRight, List<object> attrValue);
    }
}
