﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public abstract class RDBMSConnection
    {
        #region Variables
        private DbConnection coreConnection;
        private RDBMSCommand command;
        
        protected DbConnection CoreConnection
        {
            get { return coreConnection; }
            set { coreConnection = value; }
        }

        public RDBMSCommand Command
        {
            get { return command; }
            protected set { command = value; }
        }

        #endregion

        #region Constructors
        public RDBMSConnection(string connectionString)
        {
            CoreConnection = GetConnection(connectionString);
            Command = GetCommand();
        }
        #endregion

        #region Methods

        #region Execute Command
        public DataTable GetSchema(string schema)
        {
            CoreConnection.Open();
            DataTable dataTable = CoreConnection.GetSchema(schema);
            CoreConnection.Close();
            return dataTable;
        }

        public DataTable ExecuteQuery(string commandText)
        {
            CoreConnection.Open();
            DbCommand dbCommand = CoreConnection.CreateCommand();
            dbCommand.CommandText = commandText;
            DbDataReader dbDataReader = dbCommand.ExecuteReader();
            DataTable result = new DataTable();
            result.Load(dbDataReader);
            CoreConnection.Close();
            return result;
        }

        public int ExecuteNonQuery(string commandText)
        {
            CoreConnection.Open();
            DbCommand dbCommand = CoreConnection.CreateCommand();
            dbCommand.CommandText = commandText;
            int result = dbCommand.ExecuteNonQuery();
            CoreConnection.Close();
            return result;
        }

        public object ExecuteScalar(string commandText)
        {
            CoreConnection.Open();
            DbCommand dbCommand = CoreConnection.CreateCommand();
            dbCommand.CommandText = commandText;
            object result = dbCommand.ExecuteScalar();
            CoreConnection.Close();
            return result;
        }
        #endregion
        
        public string GetConnectionString()
        {
            return CoreConnection.ConnectionString;
        }

        protected abstract DbConnection GetConnection(string connectionString);
        protected abstract RDBMSCommand GetCommand();

        public abstract string ClassName();
        #endregion
    }
}
