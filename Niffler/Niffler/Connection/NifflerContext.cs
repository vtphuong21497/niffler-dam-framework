﻿using NifflerConnection.ORM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Attribute = NifflerConnection.ORM.Attribute;

namespace NifflerConnection.Connection
{
    public class NifflerContext
    {
        private RDBMSConnection Connection;
        private Database database;
        private List<DbTable> tables = new List<DbTable>();

        public NifflerContext(RDBMSConnection connection)
        {
            Connection = connection;
            database = (new ORM.ORM(Connection)).Mapping();
        }

        public Table GetTable(string name)
        {
            return database[name];
        }

        public void Register(DbTable table)
        {
            tables.Add(table);
            table.Connection = Connection;
            if (database.Tables.ContainsKey(table.GetTypeDescription()))
                table.Table = database[table.GetTypeDescription()];
            else
                table.Table = null;
        }

        public SQLBuilder GetBuilder()
        {
            return new SQLBuilder(Connection.Command);
        }

        private List<T> FillData<T>(DataTable dataTable)
        {
            List<T> results = new List<T>();
            
            foreach (DataRow dr in dataTable.Rows)
            {
                T record;
                if (database.Tables.ContainsKey(typeof(T).Name))
                {
                    record = (T)Activator.CreateInstance(typeof(T), new object[] { dr, this });
                    results.Add(record);
                } else
                {
                    record = (T)Activator.CreateInstance(typeof(T), new object[] { });
                    PropertyInfo[] tmp = typeof(T).GetProperties();
                    int idx = 0;
                    foreach (var info in tmp)
                    {
                        if (info.PropertyType.Name.Contains('`'))
                            continue;
                        info.SetValue(record, dr[idx++]);
                    }
                }

            }
                
            return results;
        }

        private object CreateDatabaseClass(Type type, DataRow dr, ref int idx)
        {
            DataTable dt = database.Tables[type.Name].GetDataTable();
            DataRow row = dt.NewRow();

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                row[i] = dr[idx++];
            }

            return Activator.CreateInstance(type, new object[] { dr, this });
        }

        private object CreateInstance(Type type, DataRow dr, ref int idx)
        {
            object record;
            if (!type.Name.Contains("AnonymousType") && !type.Name.Contains("Nullable"))
            {
                if (database.Tables.ContainsKey(type.Name))
                    return CreateDatabaseClass(type, dr, ref idx);

                record = Activator.CreateInstance(type, new object[] { });

                PropertyInfo[] tmp = type.GetProperties();
                foreach (var info in tmp)
                {
                    if (info.PropertyType.Name.Contains('`'))
                        continue;
                    info.SetValue(record, dr[idx++]);
                }
                return record;
            }

            List<object> p = new List<object>();
            PropertyInfo[] infos = type.GetProperties();
            for (int i = 0; i < infos.Length; i++)
            {
                if (!TypeMapper.Instance.BasicType(infos[i].PropertyType))
                {
                    p.Add(CreateInstance(infos[i].PropertyType, dr, ref idx));
                }
                else
                {
                    if (dr[idx] == DBNull.Value)
                    {
                        p.Add(null);
                        idx++;
                        continue;
                    }
                    p.Add(dr[idx++]);
                }
            }
            record = Activator.CreateInstance(type, p.ToArray());
            return record;
        }

        private List<T> FillData<T>(Type type, DataTable data)
        {
            List<T> result = new List<T>();

            //List<List<string>> prop = GetListProperties(new List<string>(), pattern.GetType());

            //if (prop.Count != data.Columns.Count)
            //    throw new Exception("Can't fill data, please check pattern again!");

            for (int i = 0; i < data.Rows.Count; i++)
            {
                int idx = 0;
                //T record = CreateInstance<T>(pattern, data.Rows[i], ref idx);
                T record = (T)CreateInstance(type, data.Rows[i], ref idx);
                result.Add(record);
            }

            return result;
        }

        public List<T> ExecuteQuery<T>(string sqlCommand)
        {
            DataTable dt = Connection.ExecuteQuery(sqlCommand);
            return FillData<T>(typeof(T), dt);
        }

        public List<T> ExecuteQuery<T>(T pattern, string sqlCommand)
        {
            DataTable dt = Connection.ExecuteQuery(sqlCommand);
            return FillData<T>(pattern.GetType(), dt);
        }

        public DataTable ExecuteQuery(string sqlCommand)
        {
            return Connection.ExecuteQuery(sqlCommand);
        }

        public object ExecuteScalar(string sqlCommand)
        {
            return Connection.ExecuteScalar(sqlCommand);
        }

        public int ExecuteNonQuery(string sqlCommand)
        {
            return Connection.ExecuteNonQuery(sqlCommand);
        }

        public List<object> GetListAttributeValues<T>(T element, List<string> attrNames)
        {
            List<object> results = new List<object>();
            foreach (var attrName in attrNames)
            {
                results.Add(typeof(T).GetProperty(attrName).GetValue(element));
            }
            return results;
        }

        public List<Tres> GetForeignKeyData<Targ, Tres>(Targ element, string tableName, string refName)
        {
            if (!database.Tables.ContainsKey(tableName) ||
                    !database.Tables.ContainsKey(refName)) return null;
            var relas = database[tableName].Relationships;
            foreach (var rela in relas.Values)
            {
                if (rela.Table.Name == refName)
                {
                    List<string> attrLeft = rela.Detail.GetListAttributes(tableName);
                    List<string> attrRight = rela.Detail.GetListAttributes(refName);
                    List<object> attrValue = GetListAttributeValues<Targ>(element, attrLeft);

                    string command = Connection.Command.GetJoinCommand(tableName, refName, attrLeft, attrRight, attrValue);
                    DataTable dataTable = Connection.ExecuteQuery(command);
                    return FillData<Tres>(dataTable);
                }
            }
            return null;
        }       

        internal bool ContainsTable(string tableName)
        {
            return database.Tables.ContainsKey(tableName);
        }

        public int SaveChanges()
        {
            int total = 0;
            foreach (var table in tables)
                total += table.SaveChanges();
            
            return total;
        }

        public NifflerQueryable<T> GetQueryable<T>(DbTable<T> dbTable) where T : class
        {
            var x = new NifflerQueryable<T>();
            x.context = this;
            return x;
        }
    }
}
