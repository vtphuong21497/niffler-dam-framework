﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NifflerConnection.ORM;

namespace NifflerConnection.Connection
{
    // DbTable used storage data on local

    public abstract class DbTable
    {
        internal RDBMSConnection Connection;
        internal Table Table;

        public DbTable(NifflerContext context)
        {
            context.Register(this);
        }

        internal abstract int SaveChanges();

        internal abstract string GetTypeDescription(); 
    }

    public class DbTable<T> : DbTable where T : class
    {
        private List<DbRecord> Records = new List<DbRecord>();

        public DbTable(NifflerContext context) :
            base(context)
        {
        }

        public bool Add(T element)
        {
            if (Table != null)
            {
                Records.Add(new DbRecord(element, new DbInsert()));
                return true;
            }
            return false;
        }

        public bool Update(T element)
        {
            if (Table != null)
            {
                Records.Add(new DbRecord(element, new DbUpdate()));
                return true;
            }
            return false;
        }

        public bool Delete(T element)
        {
            if (Table != null)
            {
                Records.Add(new DbRecord(element, new DbDelete()));
                return true;
            }
            return false;
        }

        internal override int SaveChanges()
        {
            int total = 0;
            foreach(DbRecord record in Records)
            {
                total += record.Execute(Connection, Table);
            }
            Records.Clear();
            return total;
        }

        internal override string GetTypeDescription()
        {
            return typeof(T).Name;
        }
    }
}
