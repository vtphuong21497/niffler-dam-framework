﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public class ConvertHelper
    {
        private static ConvertHelper instance = null;
        public static ConvertHelper Instance
        {
            get { if (instance == null) instance = new ConvertHelper(); return instance; }
            private set { instance = value; }
        }

        protected ConvertHelper()
        {
            FixToString = new HashSet<Type>();
            FixToString.Add(typeof(string));
            FixToString.Add(typeof(DateTime));
        }

        private HashSet<Type> FixToString;
            
        public string SQLServerToString(object value)
        {
            Type type = value.GetType();
            string result;
            if (FixToString.Contains(type)) {
                result = "'" + value.ToString() + "'";
                if (type == typeof(string))
                    result = 'N' + result;
            }
            else
                result = value.ToString();
            return result;
        }   
    }
}
