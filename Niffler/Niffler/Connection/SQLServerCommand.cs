﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.Connection
{
    public class SQLServerCommand : RDBMSCommand
    {
        #region Override Methods
        public override string ToString(object obj)
        {
            return ConvertHelper.Instance.SQLServerToString(obj);
        }

        public override string GetRetrieveForeignKeyCommand()
        {
            string commandText = @"
                SELECT
                    f.name as constraint_name
                    ,OBJECT_NAME(f.parent_object_id) referencing_table_name
                    ,COL_NAME(fc.parent_object_id, fc.parent_column_id) referencing_column_name
                    ,OBJECT_NAME (f.referenced_object_id) referenced_table_name
                    ,COL_NAME(fc.referenced_object_id, fc.referenced_column_id) referenced_column_name
                FROM 
                    sys.foreign_keys AS f
                    INNER JOIN sys.foreign_key_columns AS fc ON f.object_id = fc.constraint_object_id";
            return commandText;
        }

        public override string GetRetrievePrimaryKeyCommand()
        {
            string commandText = @"
                SELECT 
	                o.Name AS 'Table_Name'
	                ,c.Name AS 'Column_Name'
                FROM 
                    sys.indexes i
                    INNER JOIN sys.index_columns ic ON i.object_id = ic.object_id AND i.index_id = ic.index_id
                    INNER JOIN sys.columns c ON ic.object_id = c.object_id and ic.column_id = c.column_id
                    INNER JOIN sys.objects o ON i.object_id = o.object_id
                    INNER JOIN sys.schemas sc ON o.schema_id = sc.schema_id
                WHERE i.is_primary_key = 1
                ORDER BY o.Name, i.Name";
            return commandText;
        }

        public override string GetRetrieveAttributeOfTableCommand(string tableName)
        {
            return "SELECT TOP 0 * FROM " + tableName;
        }

        public override string GetRetrieveDatabaseNameCommand()
        {
            return "SELECT DB_NAME()";
        }

        public override string GetInsertCommand(string tableName, List<string> attributeName, List<object> attributeValue)
        {
            string title = ConcatName(attributeName);
            string value = ConcatValue(attributeValue);

            return string.Format("INSERT INTO {0}({1}) VALUES ({2})", tableName, title, value);
        }

        public override string GetUpdateCommand(string tableName, List<string> pkName, List<object> pkValue, List<string> attrName, List<object> attrValue)
        {
            string pk = ConcatWhere(pkName, pkValue);
            string newValue = ConcatEquals(attrName, attrValue, ", ");

            return string.Format("UPDATE {0} SET {1} WHERE {2}", tableName, newValue, pk);
        }

        public override string GetDeleteCommand(string tableName, List<string> pkName, List<object> pkValue)
        {
            string pk = ConcatWhere(pkName, pkValue);

            return string.Format("DELETE {0} WHERE {1}", tableName, pk);
        }

        public override string GetJoinCommand(string leftName, string rightName, List<string> attrLeft, List<string> attrRight, List<object> attrValue)
        {
            for (int i = 0; i < attrLeft.Count; i++)
            {
                attrLeft[i] = "extend1." + attrLeft[i];
                attrRight[i] = "extend2." + attrRight[i];
            }
            string onJoin = ConcatEquals(attrLeft, attrRight, " AND ");
            string where = ConcatWhere(attrLeft, attrValue);
            return string.Format("SELECT extend2.* FROM {0} as extend1 JOIN {1} as extend2 ON ({2}) WHERE {3}", leftName, 
                rightName, onJoin, where);
        }
        #endregion
    }
}
