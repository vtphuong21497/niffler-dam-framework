﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NifflerConnection.Connection;

namespace NifflerConnection.ORM
{
    public class ORM
    {
        private RDBMSConnection connection;

        public RDBMSConnection Connection
        {
            get { return connection; }
            set { connection = value; }
        }

        public ORM(RDBMSConnection connection)
        {
            Connection = connection;
        }


        #region Add to Database
        protected virtual void AddDatabaseName(Database database)
        {
            DataTable table = connection.ExecuteQuery(
                connection.Command.GetRetrieveDatabaseNameCommand());

            if (table.Rows.Count > 0)
                database.Name = table.Rows[0][0].ToString();
        }

        protected virtual void AddPrimaryKey(Database database)
        {
            DataTable pkTable = connection.ExecuteQuery(
                connection.Command.GetRetrievePrimaryKeyCommand());
            foreach (DataRow dr in pkTable.Rows)
            {
                database[dr[0].ToString()].AddPrimaryKey(dr[1].ToString());
            }
        }

        protected virtual void AddDataTable(Database database)
        {
            DataTable dt = connection.GetSchema("Tables");
            List<string> tableNames = new List<string>();
            foreach (DataRow dr in dt.Rows)
                tableNames.Add(dr[2].ToString());

            foreach (string tableName in tableNames)
            {
                DataTable dataTable = connection.ExecuteQuery
                    (connection.Command.GetRetrieveAttributeOfTableCommand(tableName));
                database.AddTable(new Table(tableName, dataTable));
            }
        }

        protected void AddRelationship(Database database)
        {
            DataTable dataTable = connection.ExecuteQuery(
                connection.Command.GetRetrieveForeignKeyCommand());

            // name - table - column - table - column
            foreach (DataRow dr in dataTable.Rows)
            {
                string key = dr[0].ToString();
                if (!database.Relationships.ContainsKey(key))
                {
                    RelationshipDetail relationship = new RelationshipDetail();
                    relationship.Name = key;
                    relationship.ReferencingTable = database[dr[1].ToString()];
                    relationship.ReferencedTable = database[dr[3].ToString()];
                    relationship.AddAttributeMapper(dr[2].ToString(), dr[4].ToString());
                    database.Relationships.Add(key, relationship);
                }
                else
                    database.Relationships[key].AddAttributeMapper(dr[2].ToString(),
                        dr[4].ToString());
            }
        }

        #endregion

        #region Mapping
        public Database Mapping()
        {
            Database database = new Database();

            AddDatabaseName(database);
            AddDataTable(database);
            AddPrimaryKey(database);
            AddRelationship(database);
            database.BuildRelationships();

            return database;
        }
        #endregion
    }
}
