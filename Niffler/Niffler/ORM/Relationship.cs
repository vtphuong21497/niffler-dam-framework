﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.ORM
{
    public enum RelationshipType
    {
        One,
        Multiple
    }

    public class Relationship
    {
        private Table table;
        private RelationshipType type;
        private RelationshipDetail detail;

        public Table Table
        {
            get { return table; }
            set { table = value; }
        }

        public RelationshipType Type
        {
            get { return type; }
            set { type = value; }
        }

        public RelationshipDetail Detail
        {
            get { return detail; }
            set { detail = value; }
        }
    }
}
