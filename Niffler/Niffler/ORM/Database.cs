﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.ORM
{
    public class Database
    {
        #region Variables
        private string name;
        private Dictionary<string, Table> tables = new Dictionary<string, Table>();
        private Dictionary<string, RelationshipDetail> relationships = new Dictionary<string, RelationshipDetail>();

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Dictionary<string, Table> Tables
        {
            get { return tables; }
            private set { tables = value; }
        }

        public Dictionary<string, RelationshipDetail> Relationships
        {
            get { return relationships; }
            private set { relationships = value; }
        }

        public Table this[string key]
        {
            get {
                if (Tables.ContainsKey(key))
                    return Tables[key];
                else
                    return null;
            }
        }

        #endregion

        #region Methods
        public bool AddTable(Table tables)
        {
            if (Tables.ContainsKey(tables.Name))
                return false;
            Tables.Add(tables.Name, tables);
            return true;
        }

        public List<string> GetListTableNames()
        {
            return Tables.Keys.ToList();
        }

        protected List<RelationshipDetail> GetRelationshipsReferening(string referencingTableName)
        {
            if (!Tables.ContainsKey(referencingTableName))
                return null;

            List<RelationshipDetail> results = new List<RelationshipDetail>();
            foreach (RelationshipDetail rela in Relationships.Values)
            {
                if (rela.ReferencingTable.Name == referencingTableName)
                    results.Add(rela);
            }

            return results;
        }

        protected void AddRelationshipOneMultiple(Table referencing, Table referenced, RelationshipDetail detail)
        {
            referencing.AddRelationship(new Relationship()
            {
                Table = referenced,
                Type = RelationshipType.One,
                Detail = detail
            });

            referenced.AddRelationship(new Relationship()
            {
                Table = referencing,
                Type = RelationshipType.Multiple,
                Detail = detail
            });
        }

        protected void AddRelationshipMultipleMultiple(Table referencing, Table referenced, RelationshipDetail detail)
        {
            referencing.AddRelationship(new Relationship()
            {
                Table = referenced,
                Type = RelationshipType.Multiple,
                Detail = detail
            });

            referenced.AddRelationship(new Relationship()
            {
                Table = referencing,
                Type = RelationshipType.Multiple,
                Detail = detail
            });
        }

        private bool IsBuilt = false; 
        public void BuildRelationships()
        {
            if (IsBuilt)
                return;

            IsBuilt = true;
            foreach (Table referencing in Tables.Values)
            {
                var relaList = GetRelationshipsReferening(referencing.Name);
                foreach (var rela in relaList)
                {
                    Table referenced = rela.ReferencedTable;
                    AddRelationshipOneMultiple(referencing, referenced, rela);
                }
            }
        }
        #endregion

        public Database()
        {

        }
    }
}
