﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.ORM
{
    public class TypeMapper
    {
        private static TypeMapper instance = null;
        public static TypeMapper Instance
        {
            get
            {
                if (instance == null)
                    instance = new TypeMapper();
                return instance;
            }
            private set { instance = value; }
        }

        private TypeMapper()
        {
            CreateNullTypeSet();
            CreateNameMapper();
        }

        private HashSet<Type> nullType;
        private void CreateNullTypeSet()
        {
            nullType = new HashSet<Type>();
            nullType.Add(typeof(long));
            nullType.Add(typeof(bool));
            nullType.Add(typeof(DateTime));
            nullType.Add(typeof(decimal));
            nullType.Add(typeof(int));
            nullType.Add(typeof(float));
            nullType.Add(typeof(Guid));
            nullType.Add(typeof(short));
            nullType.Add(typeof(byte));
            nullType.Add(typeof(TimeSpan));
        }

        private Dictionary<Type, string> nameMapper;
        private void CreateNameMapper()
        {
            nameMapper = new Dictionary<Type, string>();
            nameMapper.Add(typeof(long), "long");
            nameMapper.Add(typeof(bool), "bool");
            nameMapper.Add(typeof(DateTime), "DateTime");
            nameMapper.Add(typeof(decimal), "decimal");
            nameMapper.Add(typeof(double), "double");
            nameMapper.Add(typeof(int), "int");
            nameMapper.Add(typeof(float), "float");
            nameMapper.Add(typeof(Guid), "Guid");
            nameMapper.Add(typeof(short), "short");
            nameMapper.Add(typeof(byte), "byte");
            nameMapper.Add(typeof(byte[]), "byte[]");
            nameMapper.Add(typeof(string), "string");
            nameMapper.Add(typeof(DateTimeOffset), "DateTimeOffset");
            nameMapper.Add(typeof(object), "object");
            nameMapper.Add(typeof(TimeSpan), "TimeSpan");
        }

        public bool BasicType(Type type)
        {
            return nameMapper.ContainsKey(type);
        }

        public string GetNameOfType(Type type, bool isNull)
        {
            string result = "";
            if (nameMapper.ContainsKey(type))
                result = nameMapper[type];
            else
                return "NoneType";

            if (isNull && nullType.Contains(type))
                result = "Nullable<" + result + ">";

            return result;
        }
    }
}
