﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data;

namespace NifflerConnection.ORM
{
    public class Table
    {
        #region Variable
        private string name;
        private Dictionary<string,Attribute> attributes = new Dictionary<string, Attribute>();
        private Dictionary<string, Relationship> relationships = new Dictionary<string, Relationship>();
        private DataTable dataTable;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Dictionary<string, Attribute> Attributes
        {
            get { return attributes; }
            private set { attributes = value; }
        }

        public Dictionary<string, Relationship> Relationships
        {
            get { return relationships; }
            private set { relationships = value; }
        }

        public Attribute this[string key]
        {
            get {
                if (Attributes.ContainsKey(key))
                    return Attributes[key];
                else
                    return null;
            }
        }
        #endregion

        #region Constructors
        public Table(string name, DataTable dataTable)
        {
            Name = name;
            int numberOfAttribute = dataTable.Columns.Count;
            for (int i = 0; i < numberOfAttribute; i++)
            {
                Attribute attribute = new Attribute();
                attribute.Name = dataTable.Columns[i].ColumnName;
                attribute.Type = dataTable.Columns[i].DataType;
                attribute.IsNull = dataTable.Columns[i].AllowDBNull;
                attribute.AutoIncrement = dataTable.Columns[i].AutoIncrement;
                
                if (!Attributes.ContainsKey(attribute.Name))
                    Attributes.Add(attribute.Name, attribute);
            }

            this.dataTable = dataTable.Clone();
            this.dataTable.Clear();
        }
        #endregion

        public bool AddRelationship(Relationship relationship)
        {
            string key = relationship.Table.Name;
            if (Relationships.ContainsKey(key)) return false;
            Relationships.Add(key, relationship);
            return true;
        }

        public bool AddPrimaryKey(string name)
        {
            if (!Attributes.ContainsKey(name))
            {                
                return false;
            }
            Attributes[name].IsPrimaryKey = true;
            return false;
        }

        public List<Attribute> GetAttributes()
        {
            return Attributes.Values.ToList();
        }

        public bool IsExistAutoIncrementColumn()
        {
            foreach (Attribute attr in GetAttributes())
            {
                if (attr.AutoIncrement)
                    return true;
            }
            return false;
        }

        public List<string> GetPrimaryKeys()
        {
            List<string> results = new List<string>();
            foreach (Attribute attr in GetAttributes())
                if (attr.IsPrimaryKey)
                    results.Add(attr.Name);
            return results;
        }

        public List<object> GetAttributeValues<T>(T element, Func<Attribute, bool> filter)
        {
            if (typeof(T).Name != Name)
                return null;

            List<object> results = new List<object>();
            foreach (Attribute attr in GetAttributes())
            {
                if (filter(attr))
                {
                    results.Add(typeof(T).GetProperty(attr.Name).GetValue(element));
                }
            }
            return results;
        }

        public DataTable GetDataTable()
        {
            return this.dataTable;
        }
    }
}
