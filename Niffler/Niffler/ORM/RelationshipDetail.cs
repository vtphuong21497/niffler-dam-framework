﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.ORM
{
    public class RelationshipDetail
    {
        #region Variables
        private string name;
        private Table referencingTable;
        private List<KeyValuePair<Attribute, Attribute>> attributeMapper =
            new List<KeyValuePair<Attribute, Attribute>>();
        private Table referencedTable;
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Table ReferencingTable
        {
            get { return referencingTable; }
            set { referencingTable = value; }
        }
        
        public Table ReferencedTable
        {
            get { return referencedTable; }
            set { referencedTable = value; }
        }

        public List<KeyValuePair<Attribute,Attribute>> AttributeMapper
        {
            get { return attributeMapper; }
            private set { attributeMapper = value; }
        }       

        #endregion

        #region Constructor

        public RelationshipDetail(Table referencingTable, Table referencedTable)
        {
            ReferencingTable = referencingTable;
            ReferencedTable = referencedTable;
        }

        public RelationshipDetail()
        {

        }

        #endregion

        #region Methods
        public bool AddAttributeMapper(string referencingTableAttributeName, 
            string referencedTableAttributeName) 
        {
            Attribute ingAttribute = ReferencingTable[referencingTableAttributeName];
            Attribute edAttribtue = ReferencedTable[referencedTableAttributeName];

            if (ingAttribute == null || edAttribtue == null)
                return false;

            attributeMapper.Add(new KeyValuePair<Attribute, Attribute>(ingAttribute, edAttribtue));
            return true;
        }

        public List<string> GetListAttributes(string name)
        {
            List<string> results = new List<string>();
            if (name == ReferencingTable.Name)
            {
                foreach (var attr in AttributeMapper)
                {
                    results.Add(attr.Key.Name);
                }
            } else if (name == ReferencedTable.Name)
            {
                foreach (var attr in AttributeMapper)
                {
                    results.Add(attr.Value.Name);
                }
            }
            return results;
        }

        #endregion
    }
}
