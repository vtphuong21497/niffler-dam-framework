﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NifflerConnection.ORM
{
    public class Attribute
    {
        private string name;
        private Type type;
        private bool isNull;
        private bool isPrimaryKey;
        private bool autoIncrement;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Type Type
        {
            get { return type; }
            set { type = value; }
        }

        public bool IsNull
        {
            get { return isNull; }
            set { isNull = value; }
        }
    
        public bool IsPrimaryKey
        {
            get { return isPrimaryKey; }
            set { isPrimaryKey = value; }
        }

        public bool AutoIncrement
        {
            get { return autoIncrement; }
            set { autoIncrement = value; }
        }

        public Attribute()
        {
            Name = "";
            Type = null;
            IsNull = true;
            IsPrimaryKey = false;
            AutoIncrement = false;
        }

        public string GetTypeName()
        {
            return TypeMapper.Instance.GetNameOfType(Type, IsNull);
        }
    }
}
